Public synthetic data for the Climate Resilience Demonstrator (CReDo). 
  
This data has been generated with the assistance of infrastructure operators in 
the UK but should not be interpreted as being representative of real assets. 
It is purely a tool for demonstrating the outputs of the CReDo project. 
  
For more details on the CReDo project, see [here](https://digitaltwinhub.co.uk/projects/credo/what-is-credo/)
